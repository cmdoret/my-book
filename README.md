# My pages

This repository showcases the deployment of a gitlab pages website from Renkulab.
Since gitlab pages cannot be used directly on Renkulab, a twin repository on gitlab.com is kept in sync with this one throught repository mirroring.

* Gitlab repository: https://gitlab.com/cmdoret/my-book
* Pages website: https://cmdoret.gitlab.io/my-book/

Detailed instructions on how to publish to gitlab pages from Renkulab [available on Discourse](https://renku.discourse.group/t/how-to-publish-to-gitlab-pages-using-renkulab/302).
